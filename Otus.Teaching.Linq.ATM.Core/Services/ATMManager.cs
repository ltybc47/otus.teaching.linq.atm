﻿using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }
        
        public IEnumerable<User> Users { get; private set; }
        
        public IEnumerable<OperationsHistory> History { get; private set; }
        
        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

    //TODO: Добавить методы получения данных для банкомата

    public User GetUser(string login, string password)
    {
      return Users.First(x => x.Login == login & x.Password == password);
    }

    public IEnumerable<Account> GetAllAccountsForUser(User user)
    {
      return Accounts.Where(acc => acc.UserId == user.Id).ToList();
    }

    public IEnumerable<OperationsHistory> GetAllHistoryForAccount(Account account)
    {
      return History.Where(hists => hists.AccountId == account.Id);
    }
    public IEnumerable<OperationsHistory> GetCustomTypeCashHistory(OperationType typeCash)
    {
      return History.Where(hists => hists.OperationType == typeCash);
    }
    public IEnumerable<User> GetUsersWhoHaveMoreN(decimal N)
    {
      return Users.Where(u1 => Accounts.Where(acc => acc.UserId == u1.Id & acc.CashAll > N).Count() > 0);
    }
  }
}