﻿using Otus.Teaching.Linq.ATM.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
  public class ATMService
  {
    private ATMManager manager;

    public ATMService(ATMManager atmManager)
    {
      this.manager = atmManager;
    }

    /// <summary>
    /// Вывод информации о заданном аккаунте по логину и паролю
    /// </summary>
    /// <param name="manager"></param>
    /// <param name="user"></param>
    /// <returns></returns>
    public  string GetInfoAboutUser( User user)
    {
      return String.Join("\n", user.GetType().GetProperties().Select(x => x.Name.ToString() + " - " + x.GetValue(user)));
    }
    /// <summary>
    /// Вывод данных о всех счетах заданного пользователя
    /// </summary>
    /// <param name="manager"></param>
    /// <param name="v1"></param>
    /// <param name="v2"></param>
    /// <returns></returns>
    public  User GetUser(string login, string password)
    {
      return manager.GetUser(login: login, password: password);
    }
    public  IEnumerable<Account> GetAllAccountsForUser( User user)
    {
      return manager.GetAllAccountsForUser(user: user);
    }

    public  string GetInfoAllAccountsForUser(User user)
    {
      var accounts = manager.GetAllAccountsForUser(user: user);

      return string.Join(
          "\n",
          accounts.Select(
            acc => string.Join(
              "\n",
              acc.GetType().GetProperties().Select(x => x.Name.ToString() + " - " + x.GetValue(acc))
              ) + "\n"
            )
          );
    }
    /// <summary>
    /// Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту
    /// </summary>
    /// <param name="manager"></param>
    /// <param name="account"></param>
    /// <returns></returns>
    public  string GetAllHistoryForAccount(IEnumerable<Account> accounts)
    {
      return
        string.Join(
          "\n",
          accounts.Select(
            acc => string.Join(
              "\n",
              manager.GetAllHistoryForAccount(acc).Select(hist =>
                string.Join(
                  "\n",
                  hist.GetType().GetProperties().Select(x => x.Name.ToString() + " - " + x.GetValue(hist))
                ) + "\n")
              )
            )
        );

    }
    /// <summary>
    /// Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта
    /// </summary>
    /// <param name="manager"></param>
    /// <param name="typeCash"></param>
    /// <returns></returns>
    public  string GetAllCashHistoryWithCustomType(OperationType typeCash)
    {
      return
        string.Join(
          "\n",
          manager.History.Where(hists => hists.OperationType == typeCash)
          .Select(hist => hist.Id + " " + hist.OperationType + " " + hist.OperationDate + " " +
          manager.Users.First(u => u.Id == manager.Accounts.First(ac => ac.Id == hist.AccountId).UserId).SurName
          )
        );
    }
    /// <summary>
    /// Вывод данных о всех пользователях у которых на счёте сумма больше N 
    /// </summary>
    /// <param name="manager"></param>
    /// <param name="N"></param>
    /// <returns></returns>
    public string GetUsersWhoHaveMoreN(decimal N)
    {
      return
        string.Join(
        "\n",
        manager.Users.Where(u1 => manager.Accounts.Where(acc => acc.UserId == u1.Id & acc.CashAll > N).Count() > 0)
        .Select(u2 => u2.SurName + " " + manager.Accounts.Where(acc => acc.UserId == u2.Id & acc.CashAll > N).Count()));
    }
  }
}
