﻿using System;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
  class Program
  {
    static void Main(string[] args)
    {
      System.Console.WriteLine("Старт приложения-банкомата...");

      var atmManager = CreateATMManager();
      var atmService = new ATMService(atmManager);

      //TODO: Далее выводим результаты разработанных LINQ запросов
      System.Console.WriteLine("\n_____\n");
      System.Console.WriteLine("1. Вывод информации о заданном аккаунте по логину и паролю;");

      System.Console.WriteLine(atmService.GetInfoAboutUser(atmService.GetUser("snow", "111")));

      System.Console.WriteLine("\n_____\n");
      System.Console.WriteLine("2. Вывод данных о всех счетах заданного пользователя;");

      System.Console.WriteLine(atmService.GetInfoAllAccountsForUser( atmService.GetUser("snow", "111")));

      System.Console.WriteLine("\n_____\n");
      System.Console.WriteLine("3. Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту; \n" +
        "Так, мы уже вычленели пользователя и все его счета, буем использовать их, тем более учимся делать цепочки вызовов \n");

      System.Console.WriteLine(
        atmService.GetAllHistoryForAccount(
          atmService.GetAllAccountsForUser(
            atmService.GetUser("snow", "111"))));

      System.Console.WriteLine("\n_____\n");
      System.Console.WriteLine("4. Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта;");

      System.Console.WriteLine(atmService.GetAllCashHistoryWithCustomType(OperationType.InputCash));

      System.Console.WriteLine("\n_____\n");
      System.Console.WriteLine("5. Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой);");

      decimal N = 9000m;

      System.Console.WriteLine(atmService.GetUsersWhoHaveMoreN(N));

      System.Console.WriteLine("\n_____\n");
      System.Console.WriteLine("Завершение работы приложения-банкомата...");
    }

    static ATMManager CreateATMManager()
    {
      using var dataContext = new ATMDataContext();
      var users = dataContext.Users.ToList();
      var accounts = dataContext.Accounts.ToList();
      var history = dataContext.History.ToList();

      return new ATMManager(accounts, users, history);
    }
  }
}